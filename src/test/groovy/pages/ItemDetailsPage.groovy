package pages

import geb.Page

/**
 * Created by pramod.nikule on 19-12-2017.
 * Item details page object
 */
class ItemDetailsPage extends Page{
    static at = { title.contains("| eBay")}
    static content = {
        itemName {$("#itemTitle")}
    }
}
