package pages

import geb.Page

/**
 * Created by pramod.nikule on 19-12-2017.
 * Ebay SignIn Page
 */
class SignInPage extends Page{
    static at = {title.contains("Sign in or Register")}
    static content = {
        username {$("#userid")}
        password {$("#pass")}
        signInButton {$("#sgnBt")}

        errorMessage {$("#errf")}
    }
}
