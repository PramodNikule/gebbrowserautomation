package pages

import geb.Page
import geb.module.Select
import modules.HeaderModule
import modules.ProfileModule

/**
 * Created by pramod.nikule on 19-12-2017.
 * Ebay Home page
 */
class EbayHomePage extends Page{
    static url = "/"
    static at = { title == "Electronics, Cars, Fashion, Collectibles, Coupons and More | eBay"}
    static content = {
        header {module HeaderModule}
        profileModule {module(ProfileModule)}
    }
}
