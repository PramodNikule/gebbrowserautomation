package pages

import geb.Page

/**
 * Created by pramod.nikule on 19-12-2017.
 * Search Result Page
 */
class SearchResultPage extends Page{
    static at = { title.contains("eBay")}
    static content = {
        itemTitle {$("a.vip", 0)}
    }
}
