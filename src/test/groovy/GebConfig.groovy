import io.appium.java_client.android.AndroidDriver
import io.appium.java_client.service.local.AppiumDriverLocalService
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.edge.EdgeDriver
import org.openqa.selenium.ie.InternetExplorerDriver
import org.openqa.selenium.remote.BrowserType
import org.openqa.selenium.remote.CapabilityType
import org.openqa.selenium.remote.DesiredCapabilities

import java.util.concurrent.TimeUnit

/**
 * Created by pramod.nikule on 18-12-2017.
 * Driver created Here
 */

waiting {
    timeout = 10
    retryInterval = 1
    includeCauseInMessage = true
}

driver = {
    DesiredCapabilities capabilities = DesiredCapabilities.chrome();
    ChromeOptions options = new ChromeOptions()
    options.addArguments("--disable-notifications");
    capabilities.setCapability(ChromeOptions.CAPABILITY, options)
    new ChromeDriver(capabilities)
}

environments {

    // To run on chrome browser, use -Dgeb.env=chrome in VM Options
    chrome {
        driver = { new ChromeDriver() }
    }

    // To run on headless chrome browser, use -Dgeb.env=chromeHeadless in VM Options
    chromeHeadless {
        driver = {
            ChromeOptions o = new ChromeOptions()
            o.addArguments('headless')
            new ChromeDriver(o)
        }
    }

    // // To run on internet explorer browser, use -Dgeb.env=ie in VM Options
    ie {
        driver = { new InternetExplorerDriver() }
    }

    // To run on Microsoft Edge browser, use -Dgeb.env=edge in VM Options
    edge {
        System.setProperty("webdriver.edge.driver","D:\\Project\\gebbrowserautomation\\MicrosoftWebDriver.exe");
        driver = { new EdgeDriver() }
    }

    //ToDo Mobile run is not yet implemented
    // To run on chrome browser of Android Mobile use -Dgeb.env=mobile in VM Options
    mobile {
        AppiumDriverLocalService service = AppiumDriverLocalService.buildDefaultService();
        if (!service.isRunning()){
            service.start()
        }
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("deviceName", "Moto E4 Plus");
        capabilities.setCapability(CapabilityType.BROWSER_NAME, BrowserType.CHROME);
        capabilities.setCapability(CapabilityType.PLATFORM, "Android");
        try {
            driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
            driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
            driver.get("https://www.facebook.com/")
        } catch (MalformedURLException e) {
            e.printStackTrace();
            service.stop();
        }
    }
}

baseUrl = "https://www.ebay.in"
reportsDir = "build/reports/GebReports"