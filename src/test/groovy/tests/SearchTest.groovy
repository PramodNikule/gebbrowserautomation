package tests

import geb.spock.GebReportingSpec
import pages.EbayHomePage
import pages.ItemDetailsPage
import pages.SearchResultPage

/**
 * Created by pramod.nikule on 19-12-2017.
 * Search item test
 */
class SearchTest extends GebReportingSpec{
    def "Searching iPhone 6"(){
        given:
        to EbayHomePage

        when:
        waitFor {at(EbayHomePage)}
        at(EbayHomePage).with {
            header.searchField.value("iPhone 6")
            header.selectCategory.setSelected("Mobile Phones")
            header.searchButton.click()
        }
        waitFor 10, {at(SearchResultPage).itemTitle.isDisplayed()}
        String itemName = at(SearchResultPage).itemTitle.text()
        at(SearchResultPage).itemTitle.click()

        then:
        assert at(ItemDetailsPage).itemName.text()==itemName
    }
}
