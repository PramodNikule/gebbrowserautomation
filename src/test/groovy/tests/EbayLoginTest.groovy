package tests

import geb.spock.GebReportingSpec
import pages.EbayHomePage
import pages.SignInPage

/**
 * Created by pramod.nikule on 19-12-2017.
 * Login Test
 */
class EbayLoginTest extends GebReportingSpec{

    def "Login with invalid credentials"() {
        given:
        to EbayHomePage

        when:
        at(EbayHomePage).header.signIn.click()
        waitFor 10, {at(SignInPage)}
        at(SignInPage).with {
            username.value("PramodNikule")
            password.value("PramodNikule@2740")
            signInButton.click()
        }

        then:
        assert at(SignInPage).errorMessage.isDisplayed()
    }

    def "Login with valid credentials"(){
        given:
        to EbayHomePage

        when:
        at(EbayHomePage).header.signIn.click()
        waitFor 10, {at(SignInPage)}
        at(SignInPage).with {
            //Enter Ebay username string
            username.value("username@gmail.com")
            //Enter Ebay password string
            password.value("password!3740")
            signInButton.click()
        }
        at(EbayHomePage).header.profileExpand.click()

        then:
        waitFor 10, {at(EbayHomePage).profileModule.username.isDisplayed()}
        //Enter Name of the signed in user
        assert at(EbayHomePage).profileModule.username.text().equals("Pramod Nikule")
    }
}
