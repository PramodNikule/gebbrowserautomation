package modules

import geb.Module

/**
 * Created by pramod.nikule on 19-12-2017.
 * Expanded profile content
 */
class ProfileModule extends Module{
    static content = {
        username {$("li#gh-un")}
        userId {$("#gh-ui")}
        accountSettings {$("a", _sp:"m570.l3399")}
        signout {$("#gh-uo")}
    }
}
