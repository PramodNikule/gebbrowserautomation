package trash

import geb.Page

/**
 * Created by pramod.nikule on 18-12-2017.
 * Snow page
 */
class SnowPage extends Page{
    static url = "snow/"
    static at = { title == "POC Ski Helmets | Snowboard Helmets | Kid's Helmets | POC Helmets | POC Goggles"}

    static content = {
        snowHeader {$("h3", text:"Snow")}
    }
}
