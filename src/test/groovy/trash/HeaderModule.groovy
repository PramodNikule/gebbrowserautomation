package trash

import geb.Module
import org.openqa.selenium.By

/**
 * Created by pramod.nikule on 18-12-2017.
 * Header of the application common throughout the application
 */
class HeaderModule extends Module{

    static content = {
        searchField {$("input#q", 0)}
        profile {$("span.icon-Account_Icon")}
        cart {$("a.mini-cart-link mini-cart-empty")}
        snow {$(By.xpath('//a[text()="Snow"]'))}
        mountainBiking {$(By.xpath('//a[text()="Mountain Biking"]'))}
        cycling {$(By.xpath('//a[text()="Cycling"]'))}
        sunglasses {$(By.xpath('//a[text()="Sunglasses"]'))}
        collections {$(By.xpath('//a[text()="Collections"]'))}
        innovation {$(By.xpath('//a[text()="Innovation"]'))}
        discoverPOC {$(By.xpath('//a[text()="Discover POC"]'))}
        searchIcon {$("button", 0, type:"submit")}

        snowModule {$(By.xpath('//a[text()="Snow"]')).module(TabModule)}
    }
}
