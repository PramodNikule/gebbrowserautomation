package trash

import geb.Page

/**
 * Created by pramod.nikule on 19-12-2017.
 * Search results will be shown in this screen
 */
class SearchResultPage extends Page{

    static at = { true }
    static content = {
        sorryMessage {$("div.content-asset", 0)}
        searchedItem {$("span.no-hits-search-term")}
    }
}
