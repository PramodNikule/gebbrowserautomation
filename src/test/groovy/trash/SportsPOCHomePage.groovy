package trash

import geb.Page

/**
 * Created by pramod.nikule on 18-12-2017.
 * Sports POC home page
 */
class SportsPOCHomePage extends Page{
    static url = "https://www.pocsports.com/"
    static at = { title == "POC | Cycling Helmets and Apparel | Snow Helmets and Goggles"}
    static content = {
        header { module(HeaderModule)}
    }
}
