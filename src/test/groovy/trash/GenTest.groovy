package trash

import geb.spock.GebReportingSpec
import trash.SearchResultPage
import trash.SnowPage
import trash.SportsPOCHomePage

/**
 * Created by pramod.nikule on 18-12-2017.
 * General Test
 */
class GenTest extends GebReportingSpec{

    def "General Test"(){
        given:
        to SportsPOCHomePage

        when:"Winterwear item is searched"
        at(SportsPOCHomePage).header.searchField.value("Winterwear")
        at(SportsPOCHomePage).header.searchIcon.click()

        then:"No item should be displayed"
        assert at(SearchResultPage).sorryMessage.text() == "We're sorry, no products were found for your search"
        assert at(SearchResultPage).searchedItem.text().equalsIgnoreCase("- Winterwear.")
    }

    def "Test Snow Page"(){
        given:
        to SportsPOCHomePage

        when:
        at(SportsPOCHomePage).header.snow.click()

        and:
        at(SportsPOCHomePage).header.snowModule.viewAll.click()

        then:
        assert at(SnowPage).snowHeader.isDisplayed()
    }
}
